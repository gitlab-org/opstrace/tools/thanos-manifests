# Thanos Receiver

The manifest is heavily taken from [`kube-thanos`](https://github.com/thanos-io/kube-thanos/tree/main/manifests).


It provides [Kustomize][1] base to deploy thanos-receiver along with a thanos-receiver-controller that
watches the stateful sets and updates the `endpoint` in `hashrings.json` of a generated configMap.
The content of the generated configMap is then used by the receiver pods.


## Environment variables and configMap expected to be provided via the base overlay

* `NAMESPACE`: Namespace where the receiver pods would be deployed. Defaults to "default" namespace.
* `OBJSTORE_CONFIG`: Configuration content of the object storage bucket. Defaults to read from `config.yaml` key in `thanos-storage` configMap.
* `HASHRINGS_CONTENT`: Configuration content of the hashrings. Defaults to read from `hashrings.json` key in `thanos-receive-generated` configMap.



## Thanos Receiver controller

The manifest is taken from examples in the controller [repo](https://github.com/observatorium/thanos-receive-controller).

### Environment variables expected to be provided via the base overlay

* `NAMESPACE`: Namespace where the receiver pods would be deployed. Defaults to "default" namespace.
* `CONFIGMAP_NAME`: Name of the configMap that holds the base `hashrings.json` content. Defaults to read from `thanos-receive-base` configMap.
* `CONFIGMAP_GENERATED_NAME`: Name of the configMap that will be generated. Defaults to `thanos-receive-generated` configMap.

