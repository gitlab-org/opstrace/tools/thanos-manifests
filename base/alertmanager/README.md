# Alertmanager manifest

A very simple manifest to deploy alertmanager.

Note: Template configuration is not available currently.

## Environment variables/configMap expected to be provided via the base overlay

* `alertmanager-config`: ConfigMap that holds the configuration for alerts.
